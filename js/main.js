let count = 0;
$('.header__button-tabs.menu').hover(() => {
    $('.fast-menu').fadeIn({
        start: function () {
            $(this).css({
                display: "flex",
            })
        }
    });

}, () => {
    if (count !== 1) {
        $('.fast-menu').fadeOut({
            start: function () {
                $(this).css({
                    display: "flex",
                })
            }
        });
        count = 0;
    }
});
$('.header__button-tabs.menu').click(() => {
    count++;
});

$('.header__date-block').click(() => {
    $('.card-block').fadeToggle({
            start: function () {
                $(this).css({
                    display: "flex",
                })
            }
        }
    )
})


$('.card-block').hover(() => {
    $('.card-block').css('overflow', 'auto');
}, () => {
    $('.card-block').css('overflow', 'hidden');

})

let bool = false;
$('.openHamburgerMenu').click(() => {
    bool = !bool;
    $('.content').css('transition', '500ms');
    $('.hamburger-menu').toggle();
    bool ? $('.content').css('margin-left', '55px') : $('.content').css('margin-left', '0');
    $('.hamburger-menu').css('width', '100px');


})

$('.hamburger-menu').hover(()=>{
    $('.hamburger-menu__tabs.logo').css('padding', '15px 32px');
    $('.hamburger-menu').css('background-color', '#10225f');
    $('.hamburger-menu').css('width', '100px');
    $('.content').css('margin-left', '105px');
    $('.hamburger-menu__footer-tabs').css('flex-direction', 'row');
    $('.hamburger-menu__tabs > .tabs-text').toggle('slide');
    $('.hamburger-menu__tabs.logo').css('border-bottom', '1px solid rgba(255, 255, 255, 0.2)');

}, ()=>{
    $('.hamburger-menu__tabs.logo').css('padding', '15px 7px');

    $('.hamburger-menu').css('width', '50px');
    $('.hamburger-menu').css('background-color', '#2c3861');
    $('.content').css('margin-left', '55px');
    $('.hamburger-menu__footer-tabs').css('flex-direction', 'column');
    $('.hamburger-menu__tabs .tabs-text').toggle('slide');
    $('.hamburger-menu__tabs.logo').css('border', 'none');
    console.log('out')
})